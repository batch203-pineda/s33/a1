// 3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())



// 4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.

.then(data => { data.map(todos => console.log(todos.title));
 console.log(data);});




// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response) => response.json())



// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.

.then(data => console.log(`The item "${data.title}" on the list has the status of ${data.completed}`));


// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos",
{
    method: "POST",
    headers: {
        "Content-Type" : "application/json"
    },
    body: JSON.stringify({
    userId: 1,
    title: "Created To Do List Item",
    completed: false
    })
}
)
    .then((response)=> response.json())
    .then(json => console.log(json));

// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
fetch("https://jsonplaceholder.typicode.com/todos/1",
{
method: "PUT",
    headers: {
        "Content-Type" : "application/json"
    },
    body: JSON.stringify({
        dateCompleted: "Pending",
        userId: 1,
        description: "To update my to do list with a different data structure",
        status: "Pending",
        title: "Updated To Do List Item",
        completed: false
    })
})
.then(response => response.json())
.then(data => console.log(data));


/*  9. Update a to do list item by changing the data structure to contain the following properties:
- Title
- Description
- Status
- Date Completed
- User ID */




// 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1",
{
method: "PATCH",
    headers: {
        "Content-Type" : "application/json"
    },
    body: JSON.stringify({
       dateCompleted: "07/09/21",
       status: "Complete"
    })
})
.then(response => response.json())
.then(json => console.log(json));



// 11. Update a to do list item by changing the status to complete and add a date when the status was changed.

// 12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "DELETE"
});

/* 13. Create a request via Postman to retrieve all the to do list items.
- GET HTTP method
- https://jsonplaceholder.typicode.com/todos URI endpoint
- Save this request as get all to do list items

DONE


14. Create a request via Postman to retrieve an individual to do list item.
- GET HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as get to do list item

DONE

15. Create a request via Postman to create a to do list item.
- POST HTTP method
- https://jsonplaceholder.typicode.com/todos URI endpoint
- Save this request as create to do list item

DONE

16. Create a request via Postman to update a to do list item.
- PUT HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as update to do list item PUT
- Update the to do list item to mirror the data structure used in the PUT fetch request

DONE

17. Create a request via Postman to update a to do list item.
- PATCH HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as create to do list item
- Update the to do list item to mirror the data structure of the PATCH fetch request

DONE

18. Create a request via Postman to delete a to do list item.
- DELETE HTTP method
- https://jsonplaceholder.typicode.com/todos/1 URI endpoint
- Save this request as delete to do list item
 

*/